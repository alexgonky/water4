<?php

spl_autoload_register(function ($className) {
    $namespace = str_replace("\\", "/", __NAMESPACE__);
    $className = str_replace("\\", "/", $className);
    $class = "" . (empty($namespace) ? "" : $namespace . "/") . "{$className}.php";
    include_once($class);
});

use Water\Water;

$message = '39470CEC20024363295C46C4';
$firstStart = 272.707;
$secondStart = 401.912;

$result = new Water($message, $firstStart, $secondStart);

print_r($result->getResult());