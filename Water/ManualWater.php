<?php

namespace water;

use water\formatters\FormatConverter;

class ManualWater
{
    private $iterator;
    private $flag;
    private $mesNum;
    private $accumulatedFirst;
    private $accumulatedSecond;


    /**
     * ManualWater constructor.
     * @param $message
     * @param $firstStart
     * @param $secondStart
     * @throws \Exception
     */
    public function __construct($message, $firstStart, $secondStart)
    {
        $this->firstStart = $firstStart;
        $this->secondStart = $secondStart;
        $this->iterator = substr($message, 0, 4);
        $this->flag = substr($message, 4, 1);
        $this->mesNum = substr($message, 5, 3);
        $this->accumulatedFirst = substr($message, 8, 20);
        $this->accumulatedSecond = substr($message, 28, 20);
    }


    /**
     * Get type of manual message
     * @return string
     */
    private function getMesType()
    {
        switch (bindec($this->mesNum)) {
            case 1:
                $type = 'Message with totals';
                break;
            case 0:
                $type = 'Test message';
                break;
            case 7:
                $type = "First message after reset";
                break;
            default:
                $type = 'Test message';
                break;
        }

        return $type;
    }

    /**
     * Get accumulated values from message
     * @return array
     */
    private function getAccValues()
    {
        return $arr = array(
            'Hot (acc)' => FormatConverter::impToM3(bindec($this->accumulatedFirst)) + $this->firstStart,
            'Cold (acc)' => FormatConverter::impToM3(bindec($this->accumulatedSecond)) + $this->secondStart
        );
    }

    /**
     * Return array with results
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            array('Type' => $this->getMesType()),
            array('Iterator' => bindec($this->iterator)),
            array('Flag Test' => bindec($this->flag)),
            array('MesNum' => bindec($this->mesNum)),
            $this->getAccValues()
        );
    }
}