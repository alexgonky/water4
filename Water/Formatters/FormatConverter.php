<?php

namespace water\formatters;


/**
 * Class FormatConverter
 * @package water\formatters
 */
class FormatConverter
{

    /**
     * Converts binary string to bit
     * @param $hexString
     * @return string
     */
    public static function strToBin(string $hexString)
    {
        $binString = '';
        for ($i = 0; $i < strlen($hexString) - 1; $i += 2) {
            $binString .= sprintf("%08d", decbin(hexdec($hexString[$i] . $hexString[$i + 1])));
        }
        return $binString;
    }

    /**
     * Convert impulse to m3
     * @param int $value
     * @return float|int
     */
    public static function impToM3(int $value)
    {
        return $value / 1000;
    }

}