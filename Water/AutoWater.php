<?php

namespace water;

use water\formatters\FormatConverter;

class AutoWater
{
    /**
     * Initial value of first channel
     * @var float
     */
    private $firstStart;

    /**
     * Initial value of second channel
     * @var float
     */
    private $secondStart;

    private $iterator;
    private $flag;
    private $accumulatedFirst;
    private $accumulatedSecond;
    private $maskFirst;
    private $maskSecond;
    private $maxHourLength;
    private $hourlyData;

    /**
     * AutoWater constructor.
     * @param $message
     * @param $firstStart
     * @param $secondStart
     */
    public function __construct($message, $firstStart, $secondStart)
    {
        $this->firstStart = $firstStart;
        $this->secondStart = $secondStart;
        $this->iterator = substr($message, 0, 4);
        $this->flag = substr($message, 4, 1);
        $this->accumulatedFirst = substr($message, 5, 17);
        $this->accumulatedSecond = substr($message, 22, 17);
        $this->maskFirst = substr($message, 39, 12);
        $this->maskSecond = substr($message, 51, 12);
        $this->maxHourLength = substr($message, 63, 3);

        $hourlyDataLength = bindec($this->maxHourLength) * substr_count($this->maskFirst . $this->maskSecond, 1);
        $this->hourlyData = substr($message, 66, $hourlyDataLength);

    }


    /**
     * Get accumulated values from message
     * @return array
     */
    private function getAccValues()
    {
        return $arr = array(
            'Hot (acc)' => FormatConverter::impToM3(bindec($this->accumulatedFirst)) + $this->firstStart,
            'Cold (acc)' => FormatConverter::impToM3(bindec($this->accumulatedSecond)) + $this->secondStart
        );
    }

    /**
     * Get hourly data from message
     * @return array
     */
    private function getHourData()
    {

        $hourData = array();
        $matches = array();
        $hourlyArr = str_split($this->hourlyData, bindec($this->maxHourLength)); //массив с почасовыми данными
        preg_match_all('/1/m', $this->maskFirst, $matches['Hot (hourly)'], PREG_OFFSET_CAPTURE); //выбираем часы с потреблением из каждого канала
        preg_match_all('/1/m', $this->maskSecond, $matches['Cold (hourly)'], PREG_OFFSET_CAPTURE); //выбираем часы с потреблением из каждого канала

        //генерируем массив с почасовым потреблением
        foreach ($matches as $key => $match) {
            if (isset($match[0])) {
                foreach ($match[0] as $partMatch) {
                    $hourData[$key][$partMatch[1] + 1] = FormatConverter::impToM3(bindec(array_shift($hourlyArr)));
                }
            }
        }

        return $hourData;
    }

    /**
     * Return array with results
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            array('Type' => 'Auto', 'Iterator' => bindec($this->iterator)),
            $this->getAccValues(),
            $this->getHourData()
        );
    }

}