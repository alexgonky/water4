<?php

namespace water;

use water\formatters\FormatConverter;

class Water
{
    public $message;
    private $content;


    /**
     * Water constructor.
     * @param $message
     * @param $firstStart
     * @param $secondStart
     * @throws \Exception
     */
    public function __construct($message, $firstStart, $secondStart)
    {
        if (strlen($message) != 24) {
            throw new \Exception('Message length not equal 12 bytes');
        }
        $this->message = FormatConverter::strToBin($message);

        //проверяем условие на длину сообщения. чтобы определить - авто или ручное
        if (strlen($this->message) > 40) {
            $class = __NAMESPACE__ . "\AutoWater";
        } else {
            $class = __NAMESPACE__ . "\ManualWater";
        }
        $this->content = new $class($this->message, (float)$firstStart, (float)$secondStart);
    }


    /**
     * Return array with results
     * @return array
     */
    public function getResult()
    {
        return $this->content->getResult();
    }

}